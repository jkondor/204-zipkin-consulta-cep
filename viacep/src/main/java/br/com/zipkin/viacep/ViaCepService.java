package br.com.zipkin.viacep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class ViaCepService {

    @Autowired
    private ViaCepFeign viaCepFeign;

    @NewSpan(name = "buscar-service")
    public HashMap<String, Object> buscar(@SpanTag("CEP") String cep) {
        return viaCepFeign.buscarCep(cep);
    }
}
