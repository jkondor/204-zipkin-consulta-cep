package br.com.zipkin.viacep;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;

@FeignClient(name = "ViaCep", url = "viacep.com.br/ws/")
public interface ViaCepFeign {

    @GetMapping("/{cep}/json")
    HashMap<String, Object> buscarCep(@PathVariable String cep);
}
